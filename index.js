import express from 'express'
import mongoose from 'mongoose'
import fs from 'fs'
import path from 'path'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import nc from 'node-cron'

import { catchNotFoundError } from './src/middlewares'
import config from './config/config'
import ewalletRoute from './src/routes/ewallet'

const app = express()

mongoose.connect(config.MONGO_URI, { useMongoClient: true })
mongoose.Promise = global.Promise

app.use(bodyParser.json({ type: '*/*' }))

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(morgan('dev'))
app.use(morgan('combined', { stream: accessLogStream }))

app.use('/ewallet', ewalletRoute)

// catch 404
app.use(catchNotFoundError)

app.listen(config.PORT || 6666, () => {  
  console.log(`Listening on port ${config.PORT || 6666}`)
})