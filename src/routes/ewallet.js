import express from 'express'
import path from 'path'

import ping from './ewallet/ping'
import register from './ewallet/register'
import getSaldo from './ewallet/getSaldo'
import getTotalSaldo from './ewallet/getTotalSaldo'
import transfer from './ewallet/transfer'
import transferTo from './ewallet/transferTo'

import { checkQuorum } from '../middlewares'

const router = express.Router()

router.post('/ping', ping) // done
router.post('/register', checkQuorum, register) // done, minus quorum
router.post('/getSaldo', checkQuorum, getSaldo) // done, minus quorum
router.post('/getTotalSaldo', checkQuorum, getTotalSaldo)
router.post('/transfer', checkQuorum, transfer)  // done, minus quorum
router.post('/transferTo', checkQuorum, transferTo)

export default router;