import axios from 'axios'
import rp from 'request-promise'

import { Customer } from '../../schema/Customer'
import { Machine } from '../../schema/Machine'
import checkUserDomicile from '../../helpers/checkUserDomicile'
import config from '../../../config/config'

export default async function(req, res, next) {
  const npm = req.body['user_id']
  // const userDomicile = await checkUserDomicile(user_id)

  Machine.findOne({ npm: npm }).exec()
  .then(machine => {
    if (machine) {
      let ip = machine.ip

      if (ip === config.THIS_NODE.IP) {      
        Machine.find({}).exec()
        .then((machines) => {
          Promise.all(machines.map(async (m) => {
            return await rp.post(`http://${m.ip}/ewallet/getSaldo`, { body: { user_id: npm }, json: true })
          }))
          .then((balances) => {
            let totalBalance = 0
            
            balances.forEach((balance) => {
              if (balance.nilai_saldo >= 0) {
                totalBalance += parseInt(balance.nilai_saldo)
              } else if (balance.nilai_saldo == -2 || balance.nilai_saldo == -4 || balance.nilai_saldo == -99) {
                return res.status(200).json({ nilai_saldo: -3, msg: `One of the node fail. Returning -2, -4, or -99` })
              }
            })
      
            return totalBalance
          })
          .then((totalBalance) => {
            return res.status(200).json({ nilai_saldo: totalBalance, msg: `Get total saldo ${npm} success` })
          })
          .catch((err) => {
            return res.status(200).json({ nilai_saldo: -3, msg: `One of the node fail. Error: ${err}` })
          })          
        })
        .catch((err) => {          
          return res.status(200).json({ nilai_saldo: -99, msg: `Message: ${err}` })          
        })
        
      } else {
        rp.post(`http://${ip}/ewallet/getTotalSaldo`, { body: { user_id: npm }, json: true })    
        .then((getTotalSaldo) => {      
          if (getTotalSaldo.nilai_saldo >= 0) {
            return res.status(200).json({ ...getTotalSaldo, msg: `Get total saldo ${npm} from ${ip} success` })
          } else {
            return res.status(200).json({ ...getTotalSaldo, msg: `Get total saldo ${npm} from ${ip} fail` })
          }
        })
        .catch((err) => {
          return res.status(200).json({ nilai_saldo: -99, msg: `Message from domicile node: ${err}` })
        })
      }
    } else {
      return res.status(200).json({
        nilai_saldo: -4,
        msg: 'user_id not found'
      })
    }
  })
  .catch(err => res.status(200).json({ nilai_saldo: -99, msg: err }))
}