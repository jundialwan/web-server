import { Customer } from '../../schema/Customer'

export default function(req, res, next) {
  const user_id = req.body['user_id']
  const amount = parseInt(req.body['nilai']) || 0

  Customer.findOne({ user_id }).exec()
  .then((customer) => {        
    if (customer) {
      if (amount >= 0 && amount <= 1000000000) {        
        customer.balance = parseInt(customer.balance) + amount
        customer.save(function(err) {
          if (err)
            return res.status(200).json({ status_transfer: -4, msg: 'Database error. Fail to save to database' })
          else
            return res.status(200).json({ status_transfer: 1, msg: 'Transfer success' })
        })
      } else {
        return res.status(200).json({ status_transfer: -5, msg: 'Transfer value not valid' })
      }
    } else {
      return res.status(200).json({ status_transfer: -1, msg: 'Customer not exist' })
    }
  })
  .catch((err) => {
    return res.status(200).json({ status_transfer: -4, msg: 'Database error. Might be wrong payload... or something else' })
  })
}