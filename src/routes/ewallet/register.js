import request from 'request'
import mongoose from 'mongoose'
import { Customer } from '../../schema/Customer'
import checkUserDomicile from '../../helpers/checkUserDomicile'
import config from '../../../config/config'

export default async function(req, res, next) {    
  const user_id = req.body['user_id']
  const name = req.body['nama']
  const userDomicile = await checkUserDomicile(user_id)

  let newCustomerSchema = { 
    user_id, 
    name, 
    balance: (userDomicile ==config.THIS_NODE.IP) ? 1000000000 : 0  
  }
  
  if (!userDomicile) {
    return res.status(200).json({ status_register: -99, msg: 'user_id not exist' })
  }
  
  // check if user exist
  Customer.findOne({ user_id })
  .then((customer) => {
    if (!customer) {
      let newCustomer = new Customer(newCustomerSchema)
      
      newCustomer.save(function(err) {
        if (err)
          return res.status(200).json({ status_register: -4, msg: 'Failed to create Customer' })
        else
          return res.status(200).json({ status_register: 1, msg: 'Register user success'})
      }) 
    } else {
      return res.status(200).json({ status_register: -4, msg: 'Customer exist' })
    }
  })
  .catch((err) => {
    return res.status(200).json({ status_register: -4, msg: 'Database error. Might be wrong payload... or something else' })
  })
}