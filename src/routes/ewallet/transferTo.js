import { Customer } from '../../schema/Customer'
import { Machine } from '../../schema/Machine'
import config from '../../../config/config'
import rp from 'request-promise'

export default async function(req, res, next) {  
  const tujuan = req.body.tujuan
  const user_id = req.body.user_id
  const nilai = parseInt(req.body.nilai)
  
  // check if destination IP exist in machine list
  let machine = await Machine.findOne({ npm: tujuan }).exec()
  
  if (!machine) {
    res.status(200).json({ status_transfer: -99, msg: 'NPM not exist in machine list' })
    return
  }

  let my_user_id = config.THIS_NODE.USER_ID
  let my_name = config.THIS_NODE.NAME
  
  // check if self exist in Customer list
  Customer.findOne({ user_id: my_user_id }).exec()
  .then((selfAccount) => {
    if (!selfAccount) {
      let newSelfAccount = new Customer({
        user_id: my_user_id,
        name: my_name,
        balance: 1000000000
      })
  
      newSelfAccount.save((err) => {
        if (err) {
          return res.status(200).json({ status_transfer: -99, msg: `Failed to save self account. Error: ${err}` })          
        }
      })
    } else {
      // check if nominal valid      
      if (parseInt(nilai) > selfAccount.balance) {
        return res.status(200).json({ status_transfer: -5, msg: 'Nominal not valid, bigger than balance' })        
      }
    }
  })
  .catch((err) => {
    res.status(200).json({ status_transfer: -99, msg: `Failed to save self account. Error: ${err}` })
  })
  
  // check if destination machine is this machine
  if (tujuan !== my_user_id) {
    rp.post(`http://${machine.ip}/ewallet/transfer`, { body: { user_id, nilai }, json: true })
    .then((transfer) => {

      if (transfer.status_transfer == -1) {

        rp.post(`http://${machine.ip}/ewallet/register`, { body: { user_id: my_user_id, nama: my_name }, json: true })
        .then((register) => {

          if (register.status_register == 1) {
            rp.post(`http://${machine.ip}/ewallet/transfer`, { body: { user_id, nilai }, json: true })
            .then((transferAgain) => {

              if (transferAgain.status_transfer == 1) {
                Customer.findOne({ user_id: my_user_id }).exec()
                .then((selfAccount)=> {

                  if (selfAccount) {
                    selfAccount.balance = parseInt(selfAccount.balance) - parseInt(nilai)
                    selfAccount.save((err) => {})

                    return res.status(200).json({ status_transfer: 1, msg: 'Transfer success' })  
                  } else {
                    return res.status(200).json({ status_transfer: -4, msg: `Transfer failed. Database error. Value not subtracted. Error ${err}` })  
                  }

                })
                .catch((err) => {
                  return res.status(200).json({ status_transfer: -4, msg: `Transfer failed. Database error. Value not subtracted. Error ${err}` })
                })
              } else {        
                return res.status(200).json({ ...transferAgain })
              }

            })
            .catch((err) => {
              return  res.status(200).json({ status_transfer: -99, msg: `Transfer failed. Problem while transfering in destination machine. Error: ${err}` })
            })          
          } else {
            return res.status(200).json({ status_transfer: -99, ...register, msg: 'Transfer failed. Problem while register in destination machine' })
          }

        })
        .catch((err) => {
          return res.status(200).json({ status_transfer: -99, msg: `Transfer failed. Register to destination machine failed. Error ${err}` })
        })  

      } else if (transfer.status_transfer == 1) {

        Customer.findOne({ user_id: my_user_id }).exec()
        .then((selfAccount)=> {

          if (selfAccount) {
            selfAccount.balance -= parseInt(nilai)
            selfAccount.save((err) => {})

            return res.status(200).json({ status_transfer: 1, msg: 'Transfer success' })  
          } else {
            return res.status(200).json({ status_transfer: -4, msg: `Transfer failed. Database error. Value not subtracted. Error ${err}` })  
          }

        })
        .catch((err) => {
          return res.status(200).json({ status_transfer: -4, msg: `Transfer failed. Database error. Value not subtracted. Error ${err}` })
        })

      }else {
        return res.status(200).json({ ...transfer })
      }  

    })
    .catch((err) => {
      return res.status(200).json({ status_transfer: -99, msg: `Transfer failed. Error ${err}` })
    })  
  } else {
    return res.status(200).json({ status_transfer: 1, msg: `Transfer success.` })
  }
}