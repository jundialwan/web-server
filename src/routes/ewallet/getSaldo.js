import { Customer } from '../../schema/Customer'

export default function(req, res, next) {
  const user_id = req.body['user_id']

  Customer.findOne({ user_id }).exec()
  .then((customer) => {
    if (customer) {
      return res.status(200).json({ nilai_saldo: customer.balance, mgs: `Get saldo ${user_id} success` })
    } else {
      return res.status(200).json({ nilai_saldo: -1, msg: `User ${user_id} not found` })
    }
  })
  .catch((err) => {
    return res.status(200).json({ nilai_saldo: -4, msg: `Error: ${err}` })
  })  
}