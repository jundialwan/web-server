import { Machine } from '../schema/Machine'

export default function() {  
  Machine.find({}).exec()
    .then((machines) => {
      let countUp = 0
      let nodesListLength = machines.length

      machines.forEach(e => countUp += parseInt(e.status))
      
      return countUp / nodesListLength
    })
    .then(result => console.log('Quorum ', result))
    .catch(err => console.log(err))
}