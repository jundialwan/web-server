import { Machine } from '../schema/Machine'
import config from '../../config/config';

export default async function(npm) {
  let ip = await Machine.findOne({ npm }).exec()
    .then(machine => (machine) ? machine.ip : false)
    .catch(err => false)

  return ip
}