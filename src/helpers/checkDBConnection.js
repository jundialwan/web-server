import mongoose from 'mongoose'
import config from '../../config/config'

export default function() {  
  let dbStatus = mongoose.connect(config.MONGO_URI, { useMongoClient: true })
  .then((err, res) => {
    return (err) ? false : true
  })

  return dbStatus
}