import monggose, { Schema } from 'mongoose'

const CustomerSchema = new Schema({
  user_id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  balance: {
    type: Number,
    required: true
  }
}, { collection: 'Customer' })

const Customer = monggose.model('Customer', CustomerSchema)

export { CustomerSchema, Customer }