import monggose, { Schema } from 'mongoose'

const MachineSchema = new Schema({
  ip: {
    type: String,
    required: true
  },
  npm: {
    type: String,
    required: true
  },
  status: {
    type: Number,
    required: true
  }
}, { collection: 'Machine' })

const Machine = monggose.model('Machine', MachineSchema)

export { MachineSchema, Machine }