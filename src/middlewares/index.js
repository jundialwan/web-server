import { Machine } from '../schema/Machine'

export function catchNotFoundError(req, res, next) {  
  res.status(404).send({
    status: 404,
    title: '404 Not Found',
    detail: 'URL requested not found',
  });  
}

export function checkQuorum(req, res, next) {
  Machine.find({}).exec()
  .then((machines) => {
    let countUp = 0
    let nodesListLength = machines.length

    machines.forEach(function(e) {
      countUp += e.status
    }, this)
    
    return countUp / nodesListLength
  })
  .then((quorum) => {
    console.log('Quorum', quorum)

    let endpoint = req.path  

    switch(endpoint) {
      case '/transfer':
        (quorum <= 0.5) && res.status(200).json({ status_transfer: -2, msg: `Quorum not enough ${quorum} <= 0.5` }) || next()
        break
      case '/getSaldo':
        (quorum <= 0.5) && res.status(200).json({ nilai_saldo: -2, msg: `Quorum not enough ${quorum} <= 0.5` }) || next()
        break
      case '/getTotalSaldo':
        (quorum < 1) && res.status(200).json({ nilai_saldo: -2, msg: `Quorum not enough ${quorum} < 1` }) || next()
        break
      case '/register':
        (quorum <= 0.5) && res.status(200).json({ status_register: -2, msg: `Quorum not enough ${quorum} <= 0.5` }) || next()
        break
      default:
        next()        
    }    
  })  
  .catch(err => console.log(err))
}